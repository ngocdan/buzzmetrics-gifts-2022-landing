AOS.init({
    once: true,
});

// variables
const PAGE = $("body").attr("data-page");
var $header_top = $(".header-top");
var $nav = $("nav");

const navMenu = document.getElementById("nav-menu"),
    overlay = document.querySelector(".overlay"),
    toggleMenu = document.getElementById("nav-toggle"),
    closeMenu = document.getElementById("nav-close");

/*SHOW*/
toggleMenu.addEventListener("click", () => {
    navMenu.classList.toggle("show");
    overlay.classList.add("d-block");
});

overlay.addEventListener("click", () => {
    navMenu.classList.remove("show");
    overlay.classList.remove("d-block");
});

/*HIDDEN*/
closeMenu.addEventListener("click", () => {
    navMenu.classList.remove("show");
    overlay.classList.remove("d-block");
});

/*===== ACTIVE AND REMOVE MENU User =====*/
const toggleMenuUser = document.querySelector("header .btn-user"),
    menuUser = document.querySelector("header .user-menu");
if (menuUser) {
    toggleMenuUser.addEventListener("click", (e) => {
        e.stopPropagation();
        menuUser.classList.toggle("show-user");
    });
    document.addEventListener("click", function () {
        menuUser.classList.remove("show-user");
    });
}

const navLink = document.querySelectorAll(".nav__link");

function linkAction() {
    /*Active link*/
    navLink.forEach((n) => n.classList.remove("active"));
    this.classList.add("active");

    /*Remove menu mobile*/
    navMenu.classList.remove("show");
    overlay.classList.remove("d-block");
}
navLink.forEach((n) => n.addEventListener("click", linkAction));

function removeActiveMenu(classActive) {
    menus.removeClass(classActive);
}

if (PAGE !== "homepage" || PAGE !== "introducepage") {
    //popup luckyNumber
    const btnLuckyNumber = document.querySelector(
        ".lucky-number .discover-item__link"
    );
    const overlayPopup = document.querySelector(".overlay-popup");
    const closePopupBtn = document.querySelector(
        ".luckynumber-popup .luckynumber-popup__close"
    );
    const sharePopup = document.querySelector(
        ".overlay-popup .luckynumber-popup"
    );

    if (sharePopup) {
        sharePopup.addEventListener("click", function (e) {
            e.stopPropagation();
        });
    }
    if (btnLuckyNumber) {
        btnLuckyNumber.addEventListener("click", function () {
            overlayPopup.style.visibility = "visible";
            overlayPopup.style.opacity = "1";
            overlayPopup.style.pointerEvents = "auto";
            sharePopup.style.display = "block";
            document.body.style.overflow = "hidden";
            overlayPopup.style.overflow = "hidden";
        });
    }

    if (closePopupBtn) {
        closePopupBtn.addEventListener("click", function () {
            overlayPopup.style.visibility = "hidden";
            overlayPopup.style.opacity = "0";
            overlayPopup.style.pointerEvents = "none";
            document.body.style.overflow = "initial";
            btnLuckyNumber.classList.remove("active-discover");
        });
    }

    if (overlayPopup) {
        overlayPopup.addEventListener("click", function () {
            overlayPopup.style.visibility = "hidden";
            overlayPopup.style.opacity = "0";
            overlayPopup.style.pointerEvents = "none";
            document.body.style.overflow = "initial";
            btnLuckyNumber.classList.remove("active-discover");
        });
    }

    // active discover item
    const discoverLinkAll = document.querySelectorAll(".discover-item__link");

    discoverLinkAll.forEach((n) =>
        n.addEventListener("click", function (e) {
            e.preventDefault();
            discoverLinkAll.forEach((n) =>
                n.classList.remove("active-discover")
            );
            this.classList.add("active-discover");
        })
    );

    let backtotop = document.querySelector(".discover-more");
    let footer = document.querySelector(".footer");
    let Heightfooter = footer.clientHeight;
    let getHeightWindow = window.innerHeight;
    let bodyHeight = document.documentElement.scrollHeight;

    function showBackToTop() {
        let scrollY = window.pageYOffset;
        let isFooter = scrollY + getHeightWindow < bodyHeight - Heightfooter;
        if (!isFooter) {
            backtotop.classList.add("active");
        } else {
            backtotop.classList.remove("active");
        }
    }

    document.addEventListener("scroll", showBackToTop);
}
